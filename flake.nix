{
  description = "Rendering 🍦 with two triangles";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    nixgl = { url = "github:guibou/nixGL"; inputs = { nixpkgs.follows = "nixpkgs"; flake-utils.follows = "flake-utils"; }; };
  };

  outputs = inputs@{ self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [ inputs.nixgl.overlay ];
      };
      lib = pkgs.lib;
    in {
      packages.rwwtt = with pkgs; stdenv.mkDerivation {
        name = "rwwtt";
        src = lib.cleanSource ./.;
        nativeBuildInputs = [ cmake pkgconfig ];
        buildInputs = [
          vulkan-headers
          vulkan-validation-layers
          vulkan-tools
          vulkan-loader
          glfw
          glm
          shaderc
          xorg.libX11
          xorg.libXau
          xorg.libXdmcp
        ];
        preInstall = ''
          artifacts_dir="$out/lib/rwwtt"
          mkdir -p "$artifacts_dir"
          echo shaderc is ${shaderc}
          glslc "$src/shaders/shader.vert" -o "$artifacts_dir/vert.spv"
          glslc "$src/shaders/icecream.frag" -o "$artifacts_dir/icecream.spv"
        '';
      };

      packages.default = pkgs.writeShellScriptBin "rwwtt" ''
        export RWWTT_SHADER_PATH=${self.packages.${system}.rwwtt}/lib/rwwtt
        ${pkgs.nixgl.nixVulkanIntel}/bin/nixVulkanIntel ${self.packages.${system}.rwwtt}/bin/rwwtt
      '';
    });
}
